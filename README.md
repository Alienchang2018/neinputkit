# NEInputKit

[![CI Status](https://img.shields.io/travis/1217493217@qq.com/NEInputKit.svg?style=flat)](https://travis-ci.org/1217493217@qq.com/NEInputKit)
[![Version](https://img.shields.io/cocoapods/v/NEInputKit.svg?style=flat)](https://cocoapods.org/pods/NEInputKit)
[![License](https://img.shields.io/cocoapods/l/NEInputKit.svg?style=flat)](https://cocoapods.org/pods/NEInputKit)
[![Platform](https://img.shields.io/cocoapods/p/NEInputKit.svg?style=flat)](https://cocoapods.org/pods/NEInputKit)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

NEInputKit is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'NEInputKit'
```

## Author

1217493217@qq.com, chang.liu.meme@funpuls.com

## License

NEInputKit is available under the MIT license. See the LICENSE file for more info.
